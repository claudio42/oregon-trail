//Exemplo
function Dog(name, breed, isGoodBoy) {
  this.name = name;
  this.breed = breed;
  this.isGoodBoy = isGoodBoy;
}

Dog.prototype = {
  constructor: Dog,
  sit: function () {
    // sitting code here
  },
  fetch: function () {
    // fetching code here
  },
};
//--------------------------------------------------------------

//Oregon Trail
function Traveler(name, food = 1, isHealthy = true) {
  this.name = name;
  this.food = food;
  this.isHealthy = isHealthy;
}

function Wagon(capacity, passengers = []) {
  this.capacity = capacity;
  this.passengers = passengers;
}

//Traveler
Traveler.prototype = {
  constructor: Traveler,
  hunt: function () {
    this.food += 2;
  },
  eat: function () {
    if (this.food > 0) {
      this.food -= 1;
    } else {
      this.isHealthy = false;
    }
  },
};

//Wagon
Wagon.prototype = {
  constructor: Wagon,
  getAvailableSeatCount: function () {
    let seatsLeft = this.capacity - this.passengers.length;
    return seatsLeft;
  },
  join: function (Traveler) {
    if (this.getAvailableSeatCount() > 0) {
      this.passengers.push(Traveler);
    }
  },
  shouldQuarantine: function () {
    if (this.passengers.some((passenger) => !passenger.isHealthy)) {
      return true;
    } else {
      return false;
    }
  },
  totalFood: function () {
    let foodQt = 0;
    this.passengers.forEach((passenger) => {
      foodQt += passenger.food;
    });
    return foodQt;
  },
};

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler("Henrietta");
let juan = new Traveler("Juan");
let maude = new Traveler("Maude");

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);
